<?php
$BASE_DOMAIN="https://localhost";
$FULL_ROOT_DIR=str_replace('\\','/',__DIR__);
$BASE_FOlDER="/".trim($FULL_ROOT_DIR,$_SERVER['DOCUMENT_ROOT']);
$TITLE="Test";
if(isset($_SERVER['REDIRECT_URL'])){
$redirect=$_SERVER['REDIRECT_URL'];
}else{
    $redirect="";
}
$request=substr($redirect,strlen($BASE_FOlDER));

function getAddress() {
    $protocol = $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';
    return $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}

include_once __DIR__ . '/template/top.php';

switch ($request) {
    case '/' :
        require __DIR__ . '/views/index.php';
        break;
    case '' :
        require __DIR__ . '/views/index.php';
        break;
    case '/admin' :
        require __DIR__ . '/views/admin.php';
        break;
    case '/about' :
        require __DIR__ . '/views/about.php';
        break;
    default: 
        require __DIR__ . '/views/404.php';
        break;
}

include_once __DIR__ . '/template/bottom.php';